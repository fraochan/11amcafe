# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20120727203313) do

  create_table "cities", :force => true do |t|
    t.string "name"
    t.string "country"
    t.string "locale"
  end

  add_index "cities", ["country", "locale"], :name => "index_cities_on_country_and_locale"

  create_table "matches", :force => true do |t|
    t.string   "email"
    t.string   "match_email"
    t.datetime "visited_date"
    t.integer  "match_clicked",  :default => 0
    t.integer  "match_selected", :default => 0
    t.integer  "phone_bought",   :default => 0
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "active",         :default => "A"
  end

  add_index "matches", ["active", "created_at"], :name => "index_matches_on_active_and_created_at"
  add_index "matches", ["email", "active"], :name => "index_matches_on_email_and_active"
  add_index "matches", ["email", "match_email", "active"], :name => "index_matches_on_email_and_match_email_and_active"
  add_index "matches", ["email", "visited_date"], :name => "index_matches_on_email_and_visited_date"
  add_index "matches", ["email"], :name => "index_matches_on_email"

  create_table "ratings", :force => true do |t|
    t.string   "rating_user"
    t.string   "rated_user"
    t.datetime "rated_at"
    t.integer  "rating"
    t.integer  "match_id"
  end

  add_index "ratings", ["match_id"], :name => "index_ratings_on_match_id"
  add_index "ratings", ["rating_user", "rated_user"], :name => "index_ratings_on_rating_user_and_rated_user"

  create_table "sessions", :force => true do |t|
    t.string   "session_id", :null => false
    t.text     "data"
    t.string   "email"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "sessions", ["created_at", "updated_at"], :name => "index_sessions_on_created_at_and_updated_at"
  add_index "sessions", ["session_id"], :name => "index_sessions_on_session_id"
  add_index "sessions", ["updated_at"], :name => "index_sessions_on_updated_at"

end
