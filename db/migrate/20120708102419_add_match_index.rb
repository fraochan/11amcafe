class AddMatchIndex < ActiveRecord::Migration
  def change
    add_index :matches, [:email, :match_email, :active]
    add_index :matches, [:email, :active]
    add_index :matches, [:active, :created_at]
  end
end
