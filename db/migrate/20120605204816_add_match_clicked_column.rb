class AddMatchClickedColumn < ActiveRecord::Migration
  def change
    add_column :matches, :match_clicked, :integer, :default=>0
  end
end
