class MatchesTable < ActiveRecord::Migration
  def change
    create_table :matches do |t|
      t.string :email
      t.string :match_email
      t.timestamp :visited_date
    end
    
    add_index :matches, :email
    add_index :matches, [:email, :visited_date]
  end
end
