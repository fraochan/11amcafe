class AddMatchSelectedColumn < ActiveRecord::Migration
  def change
    add_column :matches, :match_selected, :integer, :default=>0
  end
end
