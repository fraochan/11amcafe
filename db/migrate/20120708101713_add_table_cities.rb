class AddTableCities < ActiveRecord::Migration
  def change
    create_table :cities do |t|
      t.string :name
      t.string :country
      t.string :locale
    end
    
    add_index :cities, [:country,:locale]
  end
end
