class AddRatingsTable < ActiveRecord::Migration
  def change
    create_table :ratings do |t|
      t.string :rating_user
      t.string :rated_user
      t.timestamp :rated_at
      t.integer :rating
      t.integer :match_id
    end
    
    add_index :ratings, [:rating_user, :rated_user]
    add_index :ratings, :match_id
  end
end
