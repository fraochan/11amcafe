class AddPhoneBoughtColumn < ActiveRecord::Migration
  def change
	add_column :matches, :phone_bought, :integer, :default=>0
  end
end
