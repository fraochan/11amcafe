class AddTimeStampColumn < ActiveRecord::Migration
  def change
    add_column :matches, :created_at, :datetime
    add_column :matches, :updated_at, :datetime
    add_column :matches, :active, :text, :default=>'A'
  end
end
