#require 'aws/s3'


if ENV["RAILS_ENV"] && ENV["RAILS_ENV"].eql?("production") 
  AWS.config({
    :access_key_id => ''
    :secret_access_key => ''
  })
else
  AWS.config({
    :access_key_id => ''
    :secret_access_key => ''
  })
end

# DynamoDB Configuracion
  
if ENV["RAILS_ENV"] && ENV["RAILS_ENV"].eql?("production") 
  DATABASE_NAME = "11amcafe"
else
  DATABASE_NAME = "11amcafe_dev"
end

@@database = AWS::DynamoDB.new
@@user_table = @@database.tables[DATABASE_NAME].load_schema

# S3 Configuration

if ENV["RAILS_ENV"] && ENV["RAILS_ENV"].eql?("production") 
  S3_PICTURES_BUCKET = "11amcafe"
elsif ENV["RAILS_ENV"] && ENV["RAILS_ENV"].eql?("development")
  S3_PICTURES_BUCKET = "11amcafedev"
else
  S3_PICTURES_BUCKET = "11amcafetest"
end

S3_REMOTE_PATH_QUARANTINE = "Quarantine"
S3_REMOTE_PATH = "UsersPictures"

@@s3 = AWS::S3.new
