ElevenAMcafe::Application.routes.draw do
  # The priority is based upon order of creation:
  # first created -> highest priority.

  # Sample of regular route:
  #   match 'products/:id' => 'catalog#view'
  # Keep in mind you can assign values other than :controller and :action

  # Sample of named route:
  #   match 'products/:id/purchase' => 'catalog#purchase', :as => :purchase
  # This route can be invoked with purchase_url(:id => product.id)

  # Sample resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Sample resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

# matches routes  
match 'matches/new', :action =>'new', :controller => 'matches', :as=>'new', :via =>'POST'
match 'matches/show', :action =>'show', :controller =>'matches', :as=>'show', :via => 'GET'
match 'matches', :action => 'delete_match', :controller =>'matches', :as=>'delete_match', :via => 'DELETE'
# 11 am Cafe routes
match '11amcafe', :action => 'index', :controller =>'elevenAmCafe', :as => 'welcome', :via => 'GET'
match '11amcafe/customer_support', :action => 'customer_support', :controller =>'elevenAmCafe', :as => 'customer_support', :via => 'GET'
match '11amcafe/forget_password', :action => 'forget_password', :controller =>'elevenAmCafe', :as => 'forget_password', :via => 'GET'
match '11amcafe/send_forgotten_password', :action => 'send_forgotten_password', :controller =>'elevenAmCafe', :as => 'send_forgotten_password', :via => 'POST'
match '11amcafe/sweep_old_sessions', :action => 'sweep_old_sessions', :controller =>'elevenAmCafe', :via => 'GET'
match '11amcafe/get_picture/:name', :action => 'get_picture_ss', :controller => 'elevenAmCafe', :as => 'get_picture_ss', :via => 'GET'
match '11amcafe/add_picture', :action => 'add_picture_ss', :controller => 'elevenAmCafe', :as => 'add_picture_ss', :via => 'POST'
match '11amcafe/delete_picture', :action => 'delete_picture_ss', :controller => 'elevenAmCafe', :as => 'delete_picture_ss', :via => 'GET'
match '11amcafe/save_user_information', :action => 'save_user_information_ss', :controller => 'elevenAmCafe', :as => 'save_user_information_ss', :via => 'POST'

# help routes
match '11amcafe/company', :action => 'company', :controller => 'help', :as => 'company', :via => 'GET'
match '11amcafe/privacy', :action => 'privacy', :controller => 'help', :as => 'privacy', :via => 'GET'
match '11amcafe/policy', :action => 'policy', :controller => 'help', :as => 'policy', :via => 'GET'
match '11amcafe/help', :action => 'help', :controller => 'help', :as => 'help', :via => 'GET'

# user routes  
match 'users', :action => 'index', :controller => 'usersAmCafe', :as => 'users', :via => 'GET'
match 'users', :action => 'create', :controller => 'usersAmCafe', :via => 'POST'
match 'users/new', :action => 'new', :controller => 'usersAmCafe', :as => 'new_user', :via => 'GET'
#match 'users/:email/edit', :action => 'edit', :controller =>'usersAmCafe', :as => 'edit_user', :via => 'GET'
#match 'users/:email', :action => 'show', :controller => 'usersAmCafe', :as => 'user', :format=>false, :via => 'GET'
#match 'users/:email', :action => 'update', :controller => 'usersAmCafe', :via => 'PUT'
match 'users', :action => 'destroy', :controller => 'usersAmCafe', :as => 'delete', :via => 'DELETE'
match 'users/sign_in', :action => 'sign_in', :controller => 'usersAmCafe', :as => 'sign_in', :via => 'POST'
match 'users/second_step_new_user', :action => 'second_step_new_user', :controller => 'usersAmCafe', :as => 'second_step_new_user', :via => 'GET'

# member routes
match 'members', :action => 'index', :controller => 'membersAmCafe', :as => 'members', :via => 'GET'
match 'members/add_picture', :action => 'add_picture', :controller => 'membersAmCafe', :as => 'add_picture', :via => 'POST'
match 'members/get_picture/:name', :action => 'get_picture', :controller => 'membersAmCafe', :as => 'get_picture', :via => 'GET'
match 'members/delete_picture', :action => 'delete_picture', :controller => 'membersAmCafe', :as => 'delete_picture', :via => 'GET'
match 'members/validation', :action => 'validation', :controller => 'membersAmCafe', :as => 'validation', :via => 'GET'
match 'members/validate_sms', :action => 'validate_sms', :controller => 'membersAmCafe', :as => 'validate_sms', :via => 'GET'
match 'members/validate_email', :action => 'validate_email', :controller => 'membersAmCafe', :as => 'validate_email', :via => 'GET'
match 'members/validate_email_web', :action => 'validate_email_web', :controller => 'membersAmCafe', :as => 'validate_email_web', :via => 'GET'
match 'members/edit_profile', :action => 'edit_profile', :controller => 'membersAmCafe', :as => 'edit_profile', :via => 'GET'
match 'members/add_user_information', :action => 'add_user_information', :controller => 'membersAmCafe', :as => 'add_user_information', :via => 'GET'
match 'members/save_basic_info', :action => 'save_basic_info', :controller => 'membersAmCafe', :as => 'save_basic_info', :via => 'POST'
match 'members/save_new_phone', :action => 'save_new_phone', :controller =>'membersAmCafe', :as => 'save_new_phone', :via => 'POST'
match 'members/save_new_password', :action => 'save_new_password', :controller =>'membersAmCafe', :as => 'save_new_password', :via => 'POST'
match 'members/save_user_information', :action => 'save_user_information', :controller => 'membersAmCafe', :as => 'save_user_information', :via => 'POST'
match 'members/send_sms_validation', :action => 'send_sms_validation', :controller => 'membersAmCafe', :as => 'send_sms_validation', :via => 'GET'
match 'members/send_email_validation', :action => 'send_email_validation', :controller => 'membersAmCafe', :as => 'send_email_validation', :via => 'GET'
match 'members/logout', :action => 'logout', :controller => 'membersAmCafe', :as => 'logout', :via => 'GET'
match 'members/save_password', :action => 'save_password', :controller =>'membersAmCafe', :as => 'save_password', :via => 'POST'
match 'members/new_password', :action => 'new_password', :controller =>'membersAmCafe', :as => 'new_password', :via => 'GET'
match 'members/show_matches', :action => 'show_matches', :controller =>'membersAmCafe', :as => 'show_matches', :via => 'GET'
match 'members/not_activated', :action => 'not_activated', :controller =>'membersAmCafe', :as => 'not_activated', :via => 'GET'
match 'members/get_picture_match/:name', :action => 'get_picture_match', :controller =>'membersAmCafe', :as => 'get_picture_match', :via => 'GET'
match 'members/choose_match', :action => 'choose_match', :controller => 'membersAmCafe', :as => 'choose_match', :via => 'GET'
match 'members/like_match', :action => 'like_match', :controller => 'membersAmCafe', :as => 'like_match', :via => 'POST'
match 'members/customer_support', :action => 'customer_support', :controller =>'membersAmCafe', :as => 'member_customer_support', :via => 'GET'
match 'members/customer_support_finish', :action => 'customer_support_finish', :controller =>'membersAmCafe', :as => 'customer_support_finish', :via => 'GET'
match 'members/get_match_phone', :action => 'get_match_phone', :controller => 'membersAmCafe', :as => 'get_match_phone', :via => 'POST'
match 'members/save_rating', :action => 'save_rating', :controller => 'membersAmCafe', :as => 'save_rating', :via => 'POST'
match 'members/company_l', :action => 'company_l', :controller => 'membersAmCafe', :as => 'company_l', :via => 'GET'
match 'members/privacy_l', :action => 'privacy_l', :controller => 'membersAmCafe', :as => 'privacy_l', :via => 'GET'
match 'members/policy_l', :action => 'policy_l', :controller => 'membersAmCafe', :as => 'policy_l', :via => 'GET'
match 'members/help_l', :action => 'help_l', :controller => 'membersAmCafe', :as => 'help_l', :via => 'GET'
match 'members/delete_account', :action => 'delete_account', :controller => 'membersAmCafe', :as => 'delete_account', :via => 'GET'
match 'members/delete_final', :action => 'delete_final', :controller => 'membersAmCafe', :as => 'delete_final', :via => 'POST'
match 'members/suspend_final', :action => 'suspend_final', :controller => 'membersAmCafe', :as => 'suspend_final', :via => 'POST'

# admin routes
match 'admin/admin_profile', :action => 'admin_profile', :controller => 'adminAmCafe', :as =>'admin_profile', :via => 'GET'
match 'admin/save_admin_new_password', :action => 'save_admin_new_password', :controller => 'adminAmCafe', :as => 'save_admin_new_password', :via => 'POST'
match 'admin/show_invalid_male_users', :action => 'show_invalid_male_users', :controller => 'adminAmCafe', :as => 'show_invalid_male_users', :via => 'GET'
match 'admin/show_invalid_female_users', :action => 'show_invalid_female_users', :controller => 'adminAmCafe', :as => 'show_invalid_female_users', :via => 'GET'
match 'admin/validate_user', :action => 'validate_user', :controller => 'adminAmCafe', :as => 'validate_user', :via => 'POST'
match 'admin/reject_user', :action => 'reject_user', :controller => 'adminAmCafe', :as => 'reject_user', :via => 'POST'
match 'admin/get_user_picture/:name', :action => 'get_user_picture', :controller => 'adminAmCafe', :as => 'get_user_picture', :via => 'GET'

  # Sample resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Sample resource route with more complex sub-resources
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', :on => :collection
  #     end
  #   end

  # Sample resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end

  # You can have the root of your site routed with "root"
  # just remember to delete public/index.html.
  # root :to => 'welcome#index'

  # See how all your routes lay out with "rake routes"

  # This is a legacy wild controller route that's not recommended for RESTful applications.
  # Note: This route will make all actions in every controller accessible via GET requests.
  # match ':controller(/:action(/:id))(.:format)'
end
