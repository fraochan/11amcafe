class Payment < ActiveResource::Base
  self.timeout = 5
	if ENV["RAILS_ENV"] && ENV["RAILS_ENV"].eql?("production") 
		self.site = "https://login-cafe.herokuapp.com" 
	else 	
		self.site = "http://localhost:4000"
	end
end