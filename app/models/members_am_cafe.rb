
class MembersAmCafe
  def self.validate_sms(params)
	YAML.load(User.post("validate_sms", params.merge({"key"=>@@KEYLOGINCAFE})).body)
  rescue StandardError => p
    nil
  end
  
  def self.validate_email(params)
    YAML.load(User.post("validate_email", params.merge({"key"=>@@KEYLOGINCAFE})).body)
  rescue StandardError => p
    nil
  end
  
  def self.send_sms_validation(params)
    YAML.load(User.post("send_sms_validation", params.merge({"key"=>@@KEYLOGINCAFE})).body)
  rescue StandardError => p
    nil
  end
  
  def self.send_email_validation(params)
    YAML.load(User.post("send_email_validation", params.merge({"key"=>@@KEYLOGINCAFE})).body)
  rescue StandardError => p
    nil
  end
  
  def self.validation_status(email)
    YAML.load(User.post("validation_status", {"email" => email}.merge({"key"=>@@KEYLOGINCAFE})).body)
  rescue StandardError => p
    nil
  end
  
  def self.new_password(email, code, password)
    YAML.load(User.post("reset_password", {"email" => email, "code" => code, "new_password" => password}.merge({"key"=>@@KEYLOGINCAFE})).body)
  rescue StandardError => p
    nil
  end
  
  def self.is_active(email)
	  auth_response = YAML.load(User.post("is_active", {"email" => email}.merge({"key"=>@@KEYLOGINCAFE})).body)
    return true unless auth_response.nil? || auth_response["message"].nil? || auth_response["message"].eql?("false") || !auth_response.instance_of?(Hash)
    false
  rescue StandardError => p
    false
  end
  
  def self.save_new_phone(email, password, new_phone)
    return nil if email.nil? || email.eql?("") || password.nil? || password.eql?("") || new_phone.nil? || new_phone.eql?("")
    YAML.load(User.post("reset_phone", {"email" => email, "password" => password, "phone" => new_phone}.merge({"key"=>@@KEYLOGINCAFE})).body)
  rescue StandardError => p
    nil
  end
  
  def self.save_new_password(params)
    YAML.load(User.post("reset_password", params.merge({"key"=>@@KEYLOGINCAFE})).body)
  rescue StandardError => p
    nil
  end
  
  def self.get_match_phone(params)
    YAML.load(Payment.post("buy_phone", params.merge({"key"=>@@KEYLOGINCAFE})).body)
  rescue StandardError => p
    nil
  end
  
  def self.suspend_account(params)
	YAML.load(User.post("inactivate_account", params.merge({"key"=>@@KEYLOGINCAFE})).body)
  rescue StandardError => p
	nil
  end
end