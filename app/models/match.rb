
class Match < ActiveRecord::Base
  
  def self.has_match(email)
    start_time = DateTime.new(Time.zone.now.year, Time.zone.now.month, Time.zone.now.day, Time.zone.now.hour, Time.zone.now.min, Time.zone.now.sec)#+2.hours
    end_time = DateTime.new(Time.zone.now.year, Time.zone.now.month, Time.zone.now.day, 11, 0, 0)
    if start_time > end_time
      tomorrow = Time.zone.now + 1.day
      end_time = DateTime.new(tomorrow.year, tomorrow.month, tomorrow.day, 11, 0, 0)
    end
    matches = Match.where("email='#{email}' and active='A' and created_at >= to_date('#{(end_time-2.day).strftime('%Y%m%d %H:%M:%S')}','YYYYMMDD HH24:MI:SS') and created_at < to_date('#{(end_time-1.day).strftime('%Y%m%d %H:%M:%S')}','YYYYMMDD HH24:MI:SS')")
    if matches && matches.count>0
      return true
    else
      return false
    end
  end
end