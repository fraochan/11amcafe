

class UsersAmCafe
  
  attr_accessor :allowed_domains
  
	@allowed_domains=["disney.ne.jp","docomo.ne.jp","docomo.blackberry.com","emnet.ne.jp","ezweb.ne.jp","c.vodafone.ne.jp","d.vodafone.ne.jp","h.vodafone.ne.jp","k.vodafone.ne.jp","n.vodafone.ne.jp","q.vodafone.ne.jp","r.vodafone.ne.jp","s.vodafone.ne.jp","softbank.ne.jp","i.softbank.jp","t.vodafone.ne.jp","di.pdx.ne.jp","dj.pdx.ne.jp","pdx.ne.jp","willcom.com","wm.pdx.ne.jp"]

  def self.all
    @@user_table.items
  end
  
  def self.getUser(email)
    return sendResponse(@@user_table.items.at(email).attributes.to_h, []) if @@user_table.items.at(email).attributes.to_h.size>0
	return sendResponse({}, [I18n.t(:user_not_found)])
  end
  
  def self.isAdmin?(email)
  begin
    auth_response = YAML.load(User.post(:is_admin, {:email=>email}.merge({"key"=>@@KEYLOGINCAFE})).body)
    if auth_response.nil? || auth_response["message"].nil? || auth_response["message"].eql?("false") || !auth_response.instance_of?(Hash)
      return false
    end
    true
  rescue
    false
  end
  end
  
  def self.getNextToValidate
  begin
    auth_response = YAML.load(User.post(:get_next_to_validate, {:key => @@KEYLOGINCAFE}).body)
    if auth_response.nil? || auth_response["message"].nil? || auth_response["message"].eql?("false") || !auth_response.instance_of?(Hash)
      ""
    else
      auth_response["message"]
    end
  rescue
    ""
  end
  end
  
  def self.getAllToValidate
    begin
      auth_response = YAML.load(User.post(:get_all_to_validate, {:key => @@KEYLOGINCAFE}).body)
      if auth_response.nil? || auth_response["message"].nil? || auth_response["message"].eql?("false") || !auth_response.instance_of?(Hash)
        ""
      else
        auth_response["message"]
      end
    rescue
      ""
    end
  end
  
  def self.adminValidateUser(email)
    begin
      return false if !PicturesAmCafe.validateQuarantine(email)
      auth_response = YAML.load(User.post(:validate, {:email=>email}.merge({"key"=>@@KEYLOGINCAFE})).body)
      if auth_response.nil? || auth_response["message"].nil? || auth_response["message"].eql?("false") || !auth_response.instance_of?(Hash)
        return false
      end
    rescue
      false
    end
  end
  
  def self.adminRejectUser(email)
    begin
      auth_response = YAML.load(User.post(:unvalidate, {:email=>email}.merge({"key"=>@@KEYLOGINCAFE})).body)
      true
    rescue
      false
    end
  end
  
  def self.modifyInformation(email)
    begin
      auth_response = YAML.load(User.post(:modify_information, {:email=>email}.merge({"key"=>@@KEYLOGINCAFE})).body)
      true
    rescue
      false
    end
  end
  
  def self.createUser(params)
    #check params for errors
    #if params["city"].include?("All")
    #  params["city"] = ["All"]
    #end
	domain = ""
	domain = params["email"][params["email"].index("@")+1, params["email"].length] if params["email"]
    errors = checkUserData(params, ["email", "name", "phone", "age_preference", "city"])
	# checking only valid email addresses
	#errors << "E-mail not allowed" if @allowed_domains.index(domain).nil?
	errors << I18n.t(:city_field) + " " + I18n.t(:is_mandatory) if params["city"] && params["city"].eql?("0")
	return sendResponse({}, errors) if errors.size > 0
  	
    #call signup service
  	begin
	  auth_response = YAML.load(User.post(:sign_up, {:email=>params["email"], :password=>params["password"]}.merge({"key"=>@@KEYLOGINCAFE})).body)
	  
      if auth_response.nil? || auth_response["message"].nil? || auth_response["message"].eql?("false") || !auth_response.instance_of?(Hash)
        return sendResponse({}, errors << I18n.t(:user_already_in_use))
      end
    rescue StandardError => p
      return sendResponse({}, errors << p.message + "-" + auth_response)
    end
    
  	#delete unnecesary keys
  	params.delete("password2")
  	#params.delete("email2")
  	params.delete("password")
  	
  	# create record in DB
  	item = @@user_table.items.at(params["email"])
  	if item.attributes.to_h.size>0
  		return sendResponse({}, [I18n.t(:user_already_in_use)])
  	else
  	    for attempts in 0..2
  	      begin
  		      item = @@user_table.items.create(params)
  		      break
		      rescue StandardError => p
		        if attempts == 2
		          YAML.load(User.post(:delete, {:email=>params["email"], :password=>params["password"]}.merge({"key"=>@@KEYLOGINCAFE})).body)
		          return sendResponse({}, [] << p.message + " - " + I18n.t(:create_user_error))
	          end
		      end
		    end
		end    
    # return user and verification errors
  	return sendResponse(item.attributes.to_h, errors)
  end
  
  def self.updateUser(email, params, mandatory = nil)
    #if params["city"] && params["city"].include?("All")
    #  params["city"] = ["All"]
    #end
    
    #if params && params["city"]
    #  if params["city"].include?("All")
    #    params["city_en"]=["All"]
    #  else
    #    value = params["city"]
    #    if value.instance_of?(String)
    #      value = [] << value 
    #    else #if value.instance_of(Set)
    #      value = value.to_a
    #    end
    #    params["city_en"] = []
    #    value.each do |city|
    #    id = City.find_by_name_and_locale(city, I18n.locale.to_s.upcase)
    #      if id
    #        id = id.id
    #        params["city_en"] << City.find_by_id_and_locale(id, "EN").name
    #      end
    #    end
    #  end
    #end
    
    errors = checkUserData(params,mandatory)
	errors << I18n.t(:city_field) + " " + I18n.t(:is_mandatory) if params["city"] && params["city"].eql?("0")
	  return sendResponse({}, errors) if errors.size > 0
    item = @@user_table.items.at(email)
    h = item.attributes.to_h
  	if item.attributes.to_h.size>0
  	  delete_attr = {}
  	  update_attr = {}
  	  params.each_key do |key|
        item.attributes.delete(key) if h[key] && (params[key].nil? || params[key].eql?(""))
        update_attr = update_attr.merge({key => params[key]}) unless params[key].nil? || params[key].eql?("")
      end
    
  	  item.attributes.set(update_attr)
  	  item = @@user_table.items.at(email)
  	  if item.attributes.to_h.size>0
  		  @user = item.attributes.to_h 
  	    return sendResponse(@user, [])
  	  else
  	    return sendReponse({}, [I18n.t(:user_update_error)])
  	  end
  	else
  	  return sendReponse({}, [I18n.t(:user_update_error)])
  	end
  rescue StandardError => p
    return sendResponse({}, [] << p.message)
  end
  
  def self.sign_in(email, password)
    begin
      errors = []
      #oAuth = User.new
      auth_response = YAML.load(User.post("sign_in", {"email"=>email, "password"=>password}.merge({"key"=>@@KEYLOGINCAFE})).body)
      
      if auth_response.nil? || auth_response["message"].nil? || auth_response["message"].eql?("false") || !auth_response.instance_of?(Hash)
        return sendResponse({}, errors << I18n.t(:login_error))
      end
    rescue StandardError => p
      return sendResponse({}, errors << p.message)
    end
    return sendResponse({"email"=>email}, [])
  end
  
  def self.deleteUser(email)
    begin
      #oAuth = User.new
      auth_response = YAML.load(User.post("delete", {"email"=> email}.merge({"key"=>@@KEYLOGINCAFE})).body)
    rescue StandardError => p
      return sendResponse({}, errors << p.message)
    end

    # delete pictures from S3
    PicturesAmCafe.removeAllPictures(email)
    item = @@user_table.items.at(email)
  	if item.attributes.to_h.size>0
  		return sendResponse({}, []) unless item.delete
  		return sendResponse({}, [I18n.t(:user_delete_error)])
    end
    
  end
  
  def self.forget_password(email)
    begin
      auth_response = YAML.load(User.post("send_reset_password_email", {"email"=> email}.merge({"key"=>@@KEYLOGINCAFE})).body)
      if auth_response.nil? || auth_response["message"].nil? || auth_response["message"].eql?("false") || !auth_response.instance_of?(Hash)
        return sendResponse({}, [I18n.t(:wrong_email_address)])
      end
    rescue StandardError => p
      return sendResponse({}, [] << p.message)
    end
    return sendResponse({"email"=>email}, [])
  end
  
  def self.checkUserData(params, mandatory = nil)
  	errors = []
  	
  	# email format is correct
  	errors << I18n.t(:email_format_error) if params["email"] && !ValidatesEmailFormatOf::validate_email_format(params["email"]).nil?
  		
  	# 2 emails match  	
  	errors << I18n.t(:wrong_email_address) if (params["email"] && params["email"].eql?(""))# || (params["email2"] && !params["email"].eql?(params["email2"]))
  	
  	# 2 passwords match
  	errors << I18n.t('errors.format', :attribute => I18n.t(:password), :message=>I18n.t('errors.messages.confirmation')) if params["password"] && (params["password"].eql?("") || (params["password2"] && !params["password"].eql?(params["password2"])))
  	
  	# check mandatory fields
  	if !mandatory.nil?
  	 mandatory.each do |key|
  	  errors << "#{I18n.t(key.downcase.gsub(/ /, "_")+"_field")} #{I18n.t(:is_mandatory)}" unless params[key.downcase.gsub(/ /, "_")] && !params[key.downcase.gsub(/ /, "_")].eql?("")
	   end 
    end
  	
  	
  	errors
  end
  
  def self.has_advance_data?(email)
    item = @@user_table.items.at(email)
    if item.attributes.to_h.size>0 && item.attributes.to_h["I_Have(1i)"]
      true
    else
      false
    end
  end
  
  def self.is_male?(email)
    item = @@user_table.items.at(email)
    if item.attributes.to_h.size>0 && item.attributes.to_h["gender"].eql?("male") then
      true
    else
      false
    end
  end
  
   
  def self.sendResponse(user, errors)
  	response = { :status => '', :user => {}, :errors => []}
  	if errors.size == 0
  		response[:status]='OK'
  	else
  		response[:status]='ERROR'
  	end
  	response[:user]=user
  	response[:errors]=errors
  	response
  end
  
  private_class_method :checkUserData, :sendResponse
  
end