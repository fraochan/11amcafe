require 'RMagick'

class PicturesAmCafe
  
  def self.addPicture(userEmail, file, number, options)
    return false if userEmail.nil? or file.nil? or userEmail.eql?("") or number.nil? or number.eql?("")
    return false if file.size > 3000000
    origName = file.original_filename
    picName=number + File.extname(origName).downcase
    directory = Rails.root.join("public","uploads",userEmail) 
    Dir.mkdir(directory) unless Dir.exists?(directory)
    begin
      picLocalPath = File.join(directory, origName.downcase)
    
      f = File.new( picLocalPath, "wb"  )
      file.rewind
      f.write file.read
      f.close
    
      if !options.nil? and options["resize"]
        image = Magick::Image.read(picLocalPath).first
        image.change_geometry!(options["resize"]) { |cols, rows, img|
            newimg = img.resize(cols, rows)
            newimg.write(picLocalPath)
        }
      end
    rescue StandardError => p
      File.delete(picLocalPath)
      FileUtils.remove_entry_secure directory
      return false
    end
    remotePath = S3_REMOTE_PATH_QUARANTINE+"/"+userEmail+"/"+picName
    
    begin
      return s3_upload(S3_PICTURES_BUCKET, remotePath, picLocalPath) && File.delete(picLocalPath)==1
    rescue StandardError => p
      FileUtils.remove_entry_secure(directory)
      return false
    end
  ensure
    FileUtils.remove_entry_secure(directory)
  end
  
  def self.removePicture(userEmail, pictureName)
    
    return false if userEmail.nil? or pictureName.nil? or userEmail.eql?("") or pictureName.eql?("")
    
	picBucket = @@s3.buckets[S3_PICTURES_BUCKET]
	
	prefix = S3_REMOTE_PATH_QUARANTINE+"/"+userEmail+"/"+pictureName + "."
	prefix_t = S3_REMOTE_PATH_QUARANTINE+"/"+userEmail+"/"+pictureName + "_t."
	img = picBucket.objects.with_prefix(prefix).collect(&:key)[0]
    img_t = picBucket.objects.with_prefix(prefix_t).collect(&:key)[0]
	
	if !img && !img_t
		prefix = S3_REMOTE_PATH+"/"+userEmail+"/"+pictureName + "."
		prefix_t = S3_REMOTE_PATH+"/"+userEmail+"/"+pictureName + "_t."
		
		img = picBucket.objects.with_prefix(prefix).collect(&:key)[0]
		img_t = picBucket.objects.with_prefix(prefix_t).collect(&:key)[0]
    end
    # erase picture
    s3_delete(S3_PICTURES_BUCKET, img) if img
    
    # erase thumbnail
    s3_delete(S3_PICTURES_BUCKET, img_t) if img_t
    true
  rescue StandardError => p
    false
  end
  
  def self.removeAllPictures(userEmail)
    prefix = S3_REMOTE_PATH+"/"+userEmail
    picBucket = @@s3.buckets[S3_PICTURES_BUCKET]
    picBucket.objects.with_prefix(prefix).collect(&:key).each do |img|
      s3_delete(S3_PICTURES_BUCKET, img)  
    end
    prefix = S3_REMOTE_PATH_QUARANTINE+"/"+userEmail
    picBucket = @@s3.buckets[S3_PICTURES_BUCKET]
    picBucket.objects.with_prefix(prefix).collect(&:key).each do |img|
      s3_delete(S3_PICTURES_BUCKET, img)  
    end
    true
  rescue StandardError => p
    debugger
    false
  end
  
  def self.getPictureMatch(userEmail, pictureName, pictureType=nil)
  begin
	picBucket = @@s3.buckets[S3_PICTURES_BUCKET]
	
	prefix = S3_REMOTE_PATH+"/"+userEmail+"/"+pictureName + (pictureType ? "_" + pictureType : "") + "."
    
    if picBucket.objects.with_prefix(prefix).collect(&:key).count>0
      picBucket.objects[picBucket.objects.with_prefix(prefix).collect(&:key)[0]].url_for(:read, :expires => 3*60)
    else
  	  "/assets/hearts_m.png"
    end
  rescue StandardError => p
    "/assets/hearts_m.png"
  end
  end
  
  def self.getPicture(userEmail, pictureName, pictureType=nil)
  begin
	picBucket = @@s3.buckets[S3_PICTURES_BUCKET]
	
	prefix = S3_REMOTE_PATH_QUARANTINE+"/"+userEmail+"/"+pictureName + (pictureType ? "_" + pictureType : "") + "."
	if picBucket.objects.with_prefix(prefix).collect(&:key).count<=0
	  prefix = S3_REMOTE_PATH+"/"+userEmail+"/"+pictureName + (pictureType ? "_" + pictureType : "") + "."
    end
	
    if picBucket.objects.with_prefix(prefix).collect(&:key).count>0
      picBucket.objects[picBucket.objects.with_prefix(prefix).collect(&:key)[0]].url_for(:read, :expires => 3*60)
    else
  	  "/assets/hearts_m.png"
    end
  rescue StandardError => p
    "/assets/hearts_m.png"
  end
  end
  
  def self.gotPicture?(userEmail, pictureName, pictureType=nil)
    return s3_exists(S3_PICTURES_BUCKET, userEmail, pictureName, pictureType)
  end
  
  def self.getAllPictures(userEmail)
    images = []
    prefix = S3_REMOTE_PATH+"/"+userEmail
    picBucket = @@s3.buckets[S3_PICTURES_BUCKET]
    picBucket.objects.with_prefix(prefix).collect(&:key).each do |img|
      images << picBucket.objects[img].url_for(:read, :expires => 3*60)
    end
    images
  rescue StandardError => p
    []
  end
  
  def self.validateQuarantine(email)
    begin
    picBucket = @@s3.buckets[S3_PICTURES_BUCKET]

  	prefix = S3_REMOTE_PATH_QUARANTINE+"/"+email
  	picBucket.objects.with_prefix(prefix).collect(&:key).each do |item|
  	  picBucket.objects[item].move_to(S3_REMOTE_PATH + "/" + email + "/" + item[/.*\/(.*)$/,1])
	  end
	  true
	  rescue StandardError => p
	    false
	  end
  end
  
  def self.how_many_pictures?(email)
      picBucket = @@s3.buckets[S3_PICTURES_BUCKET]

    	prefix = S3_REMOTE_PATH_QUARANTINE+"/"+email
    	num = picBucket.objects.with_prefix(prefix).collect(&:key).count / 2
    	prefix = S3_REMOTE_PATH+"/"+email
    	num+=picBucket.objects.with_prefix(prefix).collect(&:key).count / 2
    	num
  end
  
  def self.s3_upload(bucketName, remotePath, fileName)
  begin
    picBucket = @@s3.buckets[bucketName]
    picBucket.objects[remotePath].delete if picBucket.objects[remotePath].exists?
    obj = picBucket.objects.create(remotePath, fileName)
    obj.write :file => fileName 
    true
  rescue
    false
  end
  end
  
  def self.s3_delete(bucketName, remotePath)
  begin
    picBucket = @@s3.buckets[bucketName]
    picBucket.objects[remotePath].delete if picBucket.objects[remotePath].exists?
    true
  rescue
    false
  end
  end
  
  def self.s3_exists(bucketName, userEmail, pictureName, pictureType)
    begin
      picBucket = @@s3.buckets[bucketName]
	  
	  #prefix = S3_REMOTE_PATH_QUARANTINE+"/"+userEmail+"/"+pictureName + (pictureType ? "_" + pictureType : "") + "."
	  #return true if picBucket.objects.with_prefix(prefix).collect(&:key).count>0
		
      prefix = S3_REMOTE_PATH+"/"+userEmail+"/"+pictureName + (pictureType ? "_" + pictureType : "") + "."

  	  if picBucket.objects.with_prefix(prefix).collect(&:key).count>0
  	    return true
  	  else
  	    return false
	    end
    rescue
      false
    end
  end
  
  private_class_method :s3_upload, :s3_delete, :s3_exists
end
