
    module MemberHelper
    
     mattr_accessor :default_chars, :default_options
     self.default_chars = ["I Am", "I Like", "I Hate", "I Have", "I Want", "A Book", "My Music", "A Movie", "Food", "Love Is", "My Job"]#I18n.t([:category_i_am,:category_i_like,:category_i_hate,:category_i_have,:category_i_want,:category_a_book,:category_my_music,:category_a_movie,:category_food,:category_love_is,:category_my_job])
     self.default_options = {:max => 5, :min => 2}
     
     def self.advance_profile_tag(prefix = nil, chars_tags = nil, options = nil, values = nil, helptext = nil)
       options = {} if options.nil?
       options = self.default_options.merge(options)
       chars_tags = chars_tags ||=self.default_chars
       content = ""
       return if chars_tags.count==0
       return unless chars_tags.instance_of?(Array)
       values = {} if values.nil?
       values = values.to_hash if !values.nil? && !values.instance_of?(Hash) && values.respond_to?(:to_hash)
       return unless values.instance_of?(Hash)
       helptext = [] unless helptext && helptext.instance_of?(Array)
       
       min = options[:min]
       max = options[:max]
       
       content = "<input type='hidden' id='#{ prefix ? prefix.to_s + "_" : "" }max' name='#{ prefix ? prefix.to_s + "[": "" }max#{ prefix ? "]" : "" }' value='#{max}'></input>".html_safe
       content += "<input type='hidden' id='#{ prefix ? prefix.to_s + "_" : "" }min' name='#{ prefix ? prefix.to_s + "[": "" }min#{ prefix ? "]" : "" }' value='#{min}'></input>".html_safe
       content += "<table class='keyword_table' cellpadding='1' cellspacing='0' border='0'><tbody>".html_safe
       content += "<tr><td colspan='2' class='title_table'><span class='large_text'>#{I18n.t(:keyword_large_text)}</span><span class='small_text'>#{I18n.t(:keyword_small_text)}</span></td></tr>".html_safe
       #content += "<tr><td colspan='2' class='title_table'><span class='large_text'>".html_safe + "キーワードを変更する" + "</span><span class='small_text'>".html_safe + "※各項目2つ以上の入力が必須です + "</span></td></tr>".html_safe
       chars_tags.each do |item|
         label = item.gsub(/ /, '_')
         simgclass = item.sub(/ /, '').downcase
         text = "category_"+label.downcase
         content += "<tr>".html_safe
         content += "<th rowspan='2' class='#{simgclass}'><p>#{I18n.t(text.to_sym)}</p></th>".html_safe #<p>#{I18n.t(text.to_sym)}</p>
         content +="<td class='inputSell'>".html_safe
         for i in 1..max
           #content += "<td>".html_safe
           #color = (i<=min) ? "#E8BBE0": ""
           content += "<input type='text' size='13' class='inputtext_kw' name='#{ prefix ? prefix.to_s + "[": "" }#{label}(#{i}i)#{ prefix ? "]" : "" }'".html_safe
           content +=" id='#{ (i<=min) ? "essencial": ""}' value='#{ values && values[label+"("+i.to_s+"i)"] ? values[label+"("+i.to_s+"i)"] : "" }'></input>".html_safe
           #content +=" id='#{ prefix ? prefix.to_s + '_': ""}#{label}_#{i}i' value='#{ values && values[label+"("+i.to_s+"i)"] ? values[label+"("+i.to_s+"i)"] : "" }'></input>".html_safe
           #content +="</td>".html_safe
         end
         content += "</td></tr>".html_safe
         content += "<tr><td class='textSell'><span class='sample'>#{I18n.t(:ex)}</span><span class='sampleKeyword'>#{helptext[chars_tags.index(item)]}</span></td></tr>".html_safe if helptext[chars_tags.index(item)]
       end
       #content += "<td><td colspan='2' class='warning'>注：不適切な内容または表現、誠意のないキーワード、<strong>個人情報（連絡先、メールアドレスなど）</strong> を入力した場合、アカウントの登録を拒否、または停止、抹消されることがあります。</td></tr>".html_safe
       content += "<tr><td colspan='2' class='warning_kw'>#{I18n.t(:keyword_warning)}</td></tr>".html_safe
       content += "<tr><td colspan='2'><input value='#{I18n.t(:save_data)}' class = 'orange button save' id='keyword_save_button' type='submit' /></td></tr>".html_safe
       content += "</tbody></table>".html_safe
       content
     end
     
     def self.category_in_a_list(h_categories, name)
       return if h_categories.nil? || name.nil? || !h_categories.instance_of?(Hash) || !name.instance_of?(String)
       result = ""
       h_categories.keys.sort.each do |key|
         if key =~ /#{name}/i
           if result.eql?("")
             result += h_categories[key]
           else
             result += ", " + h_categories[key]
           end
         end
       end
       result
     end
     
     def self.cities_select(id, name, value = nil, size = nil, city_list = I18n.t(:cities).split("-").collect {|item| item=item.strip})
       return if id.nil? || name.nil?
       if value.instance_of?(String)
         value = [] << value 
       else #if value.instance_of(Set)
         value = value.to_a
       end
       content = "<select id='#{id}' name='#{name}[]' multiple size=10 #{ size ? "style='width:"+size+"'" : ""}>".html_safe
       city_list.each { |item|
         content+="<option name='#{item}' #{ (!value.nil? && value.include?(item)) ? "selected" : ""}>#{item}</option>".html_safe
       } unless city_list.nil?
       content+= "<option name='#{I18n.t(:all_choice)}' #{ (!value.nil? && value.include?(I18n.t(:all_choice))) ? "selected" : ""}>#{I18n.t(:all_choice)}</option>".html_safe
       content+="</select>".html_safe
     end
     
     def self.show_pictures_carousel(email, match_id)
       return if match_id.nil?
       #content = "<table width='200px' height='200px'><tr><td><div id='myCarousel' class='carousel slide'><div class='carousel-inner'>".html_safe
	   content = "<div id='this-carousel-id' class='carousel slide'><div class='carousel-inner'>".html_safe
       match = Match.find_by_email_and_id(email, match_id)
       if match
         match_email = match["match_email"]
       end
       cont = 1
       (1..5).each do |i|
         if PicturesAmCafe.gotPicture?(match_email,i.to_s,"t")
					#content += "<div class='#{if cont==1 then 'active' end} item'><table width='360px' height='200px'><tr valign='middle'><td align='center'>".html_safe
					content += "<div class='#{if cont==1 then 'active imgLiquid' end} item'>".html_safe
					content += ("<a href='/members/get_picture_match/#{i}?match=#{match_id}' target='blank'><img src='/members/get_picture_match/#{i}?type=t&match=#{match_id}&token=" + ('a'..'z').to_a.shuffle[0,20].join + "' width='220px' height='230px'></img></a>").html_safe
					#content += "</td></tr></table></div>".html_safe
					content += "</div>".html_safe
					cont+=1
				 end
		   end
			 #content += "</div><a class='carousel-control left' href='#myCarousel' data-slide='prev'>&lsaquo;</a>".html_safe
			 content += "</div><a class='carousel-control left' href='#this-carousel-id' data-slide='prev'>&lsaquo;</a>".html_safe
			 #content += "<a class='carousel-control right' href='#myCarousel' data-slide='next'>&rsaquo;</a></div>".html_safe
			 content += "<a class='carousel-control right' href='#this-carousel-id' data-slide='next'>&rsaquo;</a>".html_safe
			 #content += "</td></tr></table>".html_safe
			 content += "</div>".html_safe
     end
	 
    def self.age(sBirthday)
      dob = Date.strptime(sBirthday, "%Y-%m-%d")
      today = Date.today
      age = today.year - dob.year
      age -= 1 if dob.strftime("%m%d").to_i > today.strftime("%m%d").to_i
      age
    end
    
   end
