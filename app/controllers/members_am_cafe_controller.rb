
class MembersAmCafeController < ApplicationController
  #skip_before_filter :checkCredentials, :validate_email
  layout "members"
  before_filter :checkCredentials, :except => [:validate_email, :new_password, :save_password]
  
  
  def checkCredentials
    #Session.sweep
    @ses = Session.find_by_session_id(cookies["authenticity_token"]) if cookies["authenticity_token"]
    if @ses.nil?
      flash[:error] = I18n.t(:login_error)
      cookies.permanent["authenticity_token"]=nil
      reset_session
      redirect_to welcome_path
      return
    end
    @ses.save # update session to keep member logged in (automatically logged off after 7 days of inactivity)
	session[:validation] = MembersAmCafe.validation_status(@ses[:email])
    session[:active] = (!session[:validation]["email_validation_date"].nil?) ? true : false;
	session[:valid] = session[:validation]["active"]
    session[:has_match] ||= Match.has_match(@ses.email)

    start_time = DateTime.new(Time.zone.now.year, Time.zone.now.month, Time.zone.now.day, Time.zone.now.hour, Time.zone.now.min, Time.zone.now.sec)#+2.hours
    end_time = DateTime.new(Time.zone.now.year, Time.zone.now.month, Time.zone.now.day, 11, 0, 0)
    if start_time > end_time
      tomorrow = Time.zone.now + 1.day
      end_time = DateTime.new(tomorrow.year, tomorrow.month, tomorrow.day, 11, 0, 0)
    end
    session[:end_time]=end_time
    @time_to_rematch = ((end_time - start_time) * 24 * 60 * 60).to_i
    if params["action"].eql?("show_matches") && !session[:valid]
      redirect_to welcome_path
    end
  end
  
  def add_picture
    #return unless UsersAmCafe.modifyInformation(@ses["email"])
	 session[:validation]= MembersAmCafe.validation_status(@ses[:email])
	 if params["pictures"] && params["pictures"].size <= 5
      params["pictures"].each do |key, picture|
        # add picture & thumbnail
        flash[:error] = I18n.t(:picture_upload_error) unless 
              PicturesAmCafe.addPicture(@ses["email"],picture,key,{"resize" => "640x480"}) &&
              PicturesAmCafe.addPicture(@ses["email"],picture,key+"_t",{"resize" => "210x280"})
      end
    end
    flash[:notice] = I18n.t(:picture_upload_correct) unless flash[:error]
    #if flash[:notice] && (request.xhr? || remotipart_submitted?)
    #  render :layout => false, :status => :ok
    #else
      redirect_to add_user_information_path
    #end
  end
  
  def get_picture
    url = PicturesAmCafe.getPicture(@ses["email"],params["name"], params["type"])
    redirect_to url.to_s
  end
  
  def delete_picture
    flash[:error] = I18n.t(:picture_upload_error) unless PicturesAmCafe.removePicture(@ses["email"], params["name"])
    flash[:notice] = I18n.t(:picture_upload_correct) unless flash[:error]
    redirect_to add_user_information_path
  end
  
  def validation
    
  end
  
  def validate_sms
	if params["email"] && !params["email"].eql?(@ses[:email])
		redirect_to welcome_path
		return
	end
    auth_response = MembersAmCafe.validate_sms(params) unless params.nil? or params["code"].nil?
	  if auth_response.nil? || auth_response["message"].nil? || auth_response["message"].eql?("false") || !auth_response.instance_of?(Hash)
      flash[:error] = I18n.t(:sms_validation_code_incorrect)
      redirect_to validation_path
    else
      flash[:notice] = I18n.t(:sms_validation_correct)
      redirect_to welcome_path
    end
  end
  
  def validate_email_web
	if params["email"] && !params["email"].eql?(@ses[:email])
		redirect_to welcome_path
		return
	end
    auth_response = MembersAmCafe.validate_email(params) unless params.nil? or params["code"].nil?
    if auth_response.nil? || auth_response["message"].nil? || auth_response["message"].eql?("false") || !auth_response.instance_of?(Hash)
      flash[:error] = I18n.t(:email_validation_code_incorrect)
      redirect_to validation_path
    else
      flash[:notice] = I18n.t(:email_validation_correct)
      redirect_to not_activated_path
    end
  end
  
  def validate_email
	#if params["email"] && !params["email"].eql?(@ses[:email])
	#	redirect_to welcome_path
	#	return
	#end
    auth_response = MembersAmCafe.validate_email(params) unless params.nil? or params["code"].nil?
    if auth_response.nil? || auth_response["message"].nil? || auth_response["message"].eql?("false") || !auth_response.instance_of?(Hash)
      flash[:error] = I18n.t(:email_validation_code_incorrect)
    end
    flash[:notice] = I18n.t(:email_validation_correct) + ". " + I18n.t(:must_login) unless flash[:error]
    @ses = Session.find_by_session_id(cookies["authenticity_token"])
    @ses.destroy if @ses
    reset_session
    redirect_to welcome_path
  end
  
  def index
    if session[:valid] && session[:has_match]
      redirect_to show_matches_path
    else
      #redirect_to edit_profile_path
      redirect_to not_activated_path
    end
  end
  
  def not_activated
    
  end
  
  def logout
    @ses = Session.find_by_session_id(cookies["authenticity_token"])
    @ses.destroy if @ses
    cookies.permanent["authenticity_token"]=nil
    reset_session
    redirect_to welcome_path
  end
  
  def send_sms_validation
	if params["email"] && !params["email"].eql?(@ses[:email])
		redirect_to welcome_path
		return
	end
    auth_response = MembersAmCafe.send_sms_validation(params) unless params.nil? or params["password"].nil?
    if auth_response.nil? || auth_response["message"].nil? || auth_response["message"].eql?("false") || !auth_response.instance_of?(Hash)
      flash[:error] = I18n.t(:sms_send_validation_error)
    else
      flash[:notice] = I18n.t(:sms_send_validation_correct)
    end
    redirect_to validation_path
  end
  
  def send_email_validation
	if params["email"] && !params["email"].eql?(@ses[:email])
		redirect_to welcome_path
		return
	end
    auth_response = MembersAmCafe.send_email_validation(params) unless params.nil? or params["password"].nil?
    if auth_response.nil? || auth_response["message"].nil? || auth_response["message"].eql?("false") || !auth_response.instance_of?(Hash)
      flash[:error] = I18n.t(:email_send_validation_error)
    else
      flash[:notice] = I18n.t(:email_send_validation_correct)
    end
    redirect_to validation_path
  end
  
  def new_password
	#if params["email"] && !params["email"].eql?(@ses[:email])
	#	redirect_to welcome_path
	#	return
	#end
    if (params.nil? || params["email"].nil? || params["code"].nil?)
      flash[:notice] = I18n.t(:wrong_request)
      redirect_to welcome_path
      return
    end
    @user = params
    render :layout => 'application'
  end
  
  def save_password
	#if params["email"] && !params["email"].eql?(@ses[:email])
	#	redirect_to welcome_path
	#	return
	#end
    if (params.nil? || params["email"].nil? || params["code"].nil? || params["password"].nil? || params["password2"].nil?)
      flash[:notice] = I18n.t(:wrong_request)
      redirect_to welcome_path
      return
    end
    if !params["password"].eql?(params["password2"])
      flash[:notice] = I18n.t('errors.format', :attribute => I18n.t(:password), :message=>I18n.t('errors.messages.confirmation'))
      redirect_to new_password_path({"email" => params["email"], "code" => params["code"]})
      return
    end
    auth_response = MembersAmCafe.new_password(params["email"], params["code"],params["password"])
    if auth_response.nil? || auth_response["message"].nil? || auth_response["message"].eql?("false") || !auth_response.instance_of?(Hash)
      flash[:error] = I18n.t(:password_change_error)
      redirect_to new_password_path({"email" => params["email"], "code" => params["code"]})
      return
    else
      @ses = Session.find_by_email(params["email"])
      @ses.destroy if @ses
      flash[:notice] = I18n.t(:password_change_correct) + ". " + I18n.t(:must_login) 
    end
    redirect_to welcome_path
  end
  
  def edit_profile
	  @member = UsersAmCafe.getUser(@ses[:email])[:user]
  end
  
  def add_user_information
    @member ||= UsersAmCafe.getUser(@ses[:email])[:user]
  end
  
  def save_basic_info
    @user = params["user"]
	if @user["email"] && !@user["email"].eql?(@ses[:email])
		redirect_to welcome_path
		return
	end
    birthdate = @user["birthdate(1i)"].to_s + '-' + @user["birthdate(2i)"].to_s.rjust(2,'0') + '-' + @user["birthdate(3i)"].to_s.rjust(2,'0')
    @user.delete("birthdate(1i)")
    @user.delete("birthdate(2i)")
    @user.delete("birthdate(3i)")
    @user["birthdate"]=birthdate
    email = @user["email"]
    @user.delete("email")
    response = UsersAmCafe.updateUser(email, @user, ["Name", "city"])
    if response[:status].eql?("OK")
      flash[:notice]=I18n.t(:user_update_correct)
    else
      flash[:error]=I18n.t(:user_update_error) + ". " + response[:errors].join
    end
    redirect_to edit_profile_path
  end
  
  def save_user_information
	#return unless UsersAmCafe.modifyInformation(@ses[:email])
	session[:validation]= MembersAmCafe.validation_status(@ses[:email])
	if params["email"] && !params["email"].eql?(@ses[:email])
		redirect_to welcome_path
		return
	end
    if params.nil? || params["advance"].nil?
      flash[:error] = I18n.t(:all_params_error)
      render :action => 'add_user_information'
    end
    begin
      min = params["advance"]["min"].to_i if params["advance"]["min"] && !params["advance"]["min"].eql?("")
    rescue StandardError => p
      min = 0
    end
    
    #check mandatory fields
    for i in 1..min
     params["advance"].each_key { |key|
        if key.index(i.to_s)
          if params["advance"][key].nil? || params["advance"][key].eql?("")
            flash[:error] = I18n.t(:all_params_error)
            @member = params["advance"].to_hash
            #@member["email"]=params["email"]
            render :action => 'add_user_information'
            return
          end
        end
     }
    end if min>1
    
    #email = params["email"]
    email = @ses["email"]
    params.delete("email")
    response = UsersAmCafe.updateUser(email, params["advance"]) #if UsersAmCafe.modifyInformation(email)
    if response && response[:status].eql?("OK")
      flash[:notice] = I18n.t(:user_update_correct)
    else
      flash[:error] = I18n.t(:user_update_error) + ". " + response[:errors].join.to_s
    end
    redirect_to add_user_information_path
  end
  
  def save_new_phone
	if params["user"]["email"] && !params["user"]["email"].eql?(@ses[:email])
		redirect_to welcome_path
		return
	end
    auth_response = MembersAmCafe.save_new_phone(params["user"]["email"], params["user"]["password"], params["user"]["phone"])
    if auth_response.nil? || auth_response["message"].nil? || auth_response["message"].eql?("false") || !auth_response.instance_of?(Hash)
      flash[:error] = I18n.t(:phone_change_error)
    else
      UsersAmCafe.updateUser(params["user"]["email"], {"phone" => params["user"]["phone"]})
      flash[:notice] = I18n.t(:phone_change_correct)
	  session[:active] =  nil
	  session[:valid] = nil
	  session[:has_match] = nil
	  session[:p_match] = nil
    end
    redirect_to edit_profile_path
  end
  
  def save_new_password
	if params["user"]["email"] && !params["user"]["email"].eql?(@ses[:email])
		redirect_to welcome_path
		return
	end
    if params["user"].nil? || params["user"]["email"].nil? || params["user"]["password"].nil? || params["user"]["new_password"].nil? ||
      params["user"]["new_password2"].nil? || !params["user"]["new_password"].eql?(params["user"]["new_password2"])
      flash[:error] = I18n.t(:password_change_error)
    else
      if params["user"]["password"].eql?(params["user"]["new_password"])
        flash[:notice] = I18n.t(:password_change_correct)
      else
        auth_response = MembersAmCafe.save_new_password(params["user"])
        if auth_response.nil? || auth_response["message"].nil? || auth_response["message"].eql?("false") || !auth_response.instance_of?(Hash)
          flash[:error] = I18n.t(:password_change_error)
        else
          flash[:notice] = I18n.t(:password_change_correct)
        end
      end
    end
    redirect_to edit_profile_path
  end
  
  def save_rating
	if params["email"] && !params["email"].eql?(@ses[:email])
		redirect_to welcome_path
		return
	end
    if params.nil? || params["chosen_match"].nil? || params["rating"].nil?
      flash[:error] = I18n.t(:couldnt_submit_rating)
      redirect_to show_matches_path
      return
    end
    
    match = Match.find_by_id(params["chosen_match"])
    if match.nil?
      flash[:error] = I18n.t(:couldnt_submit_rating)
      redirect_to show_matches_path({:chosen_match => params["chosen_match"]})
      return
    end
    
    rating = Rating.new
    rating.rating_user = match.email
    rating.rated_user = match.match_email
    rating.rating = params["rating"]
    rating.rated_at = Time.new
    rating.match_id = params["chosen_match"]
    rating.save
    
    flash[:info] = I18n.t(:rating_submited)
    redirect_to show_matches_path({:chosen_match => params["chosen_match"]})
    
  end
  
  def show_matches
    
    #check matches are old, so erase and get new ones (erasing match in session too)
    if session[:end_time] && Match.where("active='A' and created_at < to_date('#{(session[:end_time]-2.day).strftime('%Y%m%d %H:%M:%S')}','YYYYMMDD HH24:MI:SS')").size > 0
      session[:p_match] = nil
      session[:has_match] = nil
      
      #erase matches after 11am
      Match.update_all("active= 'B'", "active='A' and created_at < to_date('#{(session[:end_time]-2.day).strftime('%Y%m%d %H:%M:%S')}','YYYYMMDD HH24:MI:SS')")
      #matches.each do |match|
      #  match.update_attributes({:active => 'B'}, :without_protection=>true)
      #end
    end
    
    @matches = Match.where("email='#{@ses[:email]}' and active='A' and created_at >= to_date('#{(session[:end_time]-2.day).strftime('%Y%m%d %H:%M:%S')}','YYYYMMDD HH24:MI:SS') and created_at < to_date('#{(session[:end_time]-1.day).strftime('%Y%m%d %H:%M:%S')}','YYYYMMDD HH24:MI:SS')").limit(2).order(:id)
    match_chosen = Match.find_by_email_and_id(@ses[:email], params["chosen_match"])
    @chosen_match = { :id => params["chosen_match"] } if params["chosen_match"] && match_chosen
    if params["chosen_match"] && @chosen_match.nil?
      redirect_to welcome_path 
      return
    end
    if !@matches || @matches.count == 0
      session[:has_match] = nil
      #redirect_to edit_profile_path
      redirect_to not_activated_path
      return
    end
    #if match_chosen
    #  @rating = Rating.where("rating_user='#{@ses[:email]}' and rated_user='#{match_chosen.match_email}' and match_id=#{match_chosen.id}").limit(1).order("rated_at desc")
    #  if @rating.size==0
    #    @rating=nil
    #  end
    #end
    @rating = []
    @matches.each do |match|
      if !match["visited_date"]
        match.update_attributes({:visited_date => Time.now},:without_protection=>true)
      end
      rating = Rating.where("rating_user='#{@ses[:email]}' and rated_user='#{match.match_email}' and match_id=#{match.id}").limit(1).order("rated_at desc")
      if rating.size==0
          rating=nil
      end
      @rating << rating
      if match.match_selected!=0
        session[:p_match] = nil
        @chosen_match = {:id => match.id.to_s}
        match_chosen = Match.find_by_email_and_id(@ses[:email], @chosen_match[:id])
        @match_selected = match.match_selected
        #if match_chosen
        #  @rating = Rating.where("rating_user='#{@ses[:email]}' and rated_user='#{match_chosen.match_email}'").limit(1).order("rated_at desc")
        #  if @rating.size==0
        #    @rating=nil
        #  end
        #end
        @match_found = nil
        p_match = Match.where("email='#{match.match_email}' and match_email='#{match.email}' and active='A' and created_at >= to_date('#{(session[:end_time]-2.day).strftime('%Y%m%d %H:%M:%S')}','YYYYMMDD HH24:MI:SS') and created_at < to_date('#{(session[:end_time]-1.day).strftime('%Y%m%d %H:%M:%S')}','YYYYMMDD HH24:MI:SS')").limit(1)[0] unless session[:p_match]
        if p_match && p_match.match_selected!=0
          session[:p_match] = { :id => p_match.id, :show_phone => (p_match.phone_bought==1) ? true : false }
		    end
        @match_found = session[:p_match]
      end
      @match_user =  UsersAmCafe.getUser(match["match_email"])[:user] if @chosen_match && @chosen_match[:id] && @chosen_match[:id].eql?(match.id.to_s)
    end
end
  
  def get_picture_match
    match = Match.find_by_email_and_id(@ses[:email], params["match"])
    if match && match["visited_date"]
      url = PicturesAmCafe.getPictureMatch(match["match_email"],params["name"], params["type"])
      redirect_to url.to_s
    end
  end
  
  def choose_match
    match = Match.find_by_email_and_id(@ses[:email], params["match"])
    if match
      match.match_clicked=1
      match.save
    end
    redirect_to show_matches_path({:chosen_match => params["match"]})
    return
  end
  
  def like_match
	if params["email"] && !params["email"].eql?(@ses[:email])
		redirect_to welcome_path
		return
	end
    if !params || params[:id].nil?
      redirect_to welcome_path
      return
    end
    match = Match.find(params[:id])
    if !match
      redirect_to welcome_path
      return
    end
    begin
    match.match_selected = params[:id]
    match.save
    match_email = match.match_email
    match = Match.where("email='#{match_email}' and match_email='#{@ses[:email]}' and active='A' and created_at >= to_date('#{(session[:end_time]-2.day).strftime('%Y%m%d %H:%M:%S')}','YYYYMMDD HH24:MI:SS') and created_at < to_date('#{(session[:end_time]-1.day).strftime('%Y%m%d %H:%M:%S')}','YYYYMMDD HH24:MI:SS')").limit(1)[0]
    if match && match.match_clicked==0
      auth_response = YAML.load(User.post(:send_has_like_email, {:email=>match.email}.merge({"key"=>@@KEYLOGINCAFE})).body)
    elsif match && match.match_clicked==1 && match.match_selected && match.match_selected.to_s.eql?(match.id)
      auth_response = YAML.load(User.post(:send_has_match_email, {:email=>match.email}.merge({"key"=>@@KEYLOGINCAFE})).body)
    end
    rescue StandardError => p
      flash[:notice] = I18n.t(:error_like_match)
      redirect_to show_matches_path
    end 
    redirect_to show_matches_path
  end
  
  def get_match_phone
	if params["email"] && !params["email"].eql?(@ses[:email])
		redirect_to welcome_path
		return
	end
	auth_response = MembersAmCafe.get_match_phone(params) unless params.nil?
 	  if auth_response.nil? || auth_response["message"].nil? || auth_response["message"].eql?("false") || !auth_response.instance_of?(Hash)
      flash[:error] = I18n.t(:get_match_phone_error)
	  else
		  match = Match.find_by_id(params[:id])
		  if match
				match.phone_bought = 1
				match.save
				session[:p_match]=nil
		  end
    end
    redirect_to show_matches_path
  end
  
  def customer_support
  end
  
  def customer_support_finish
  end
  
    
  def help_l
  end
  
  def policy_l
  end
  
  def privacy_l
  end
  
  def company_l
  end
  
  def delete_account
  
  end
  
  def delete_final
	# delete from DB (archive somewhere else ???)
	# erase session
	# logout
	if params["email"] && !params["email"].eql?(@ses[:email])
		redirect_to welcome_path
		return
	end
	response = UsersAmCafe.deleteUser(params["email"])
	if response[:errors] && response[:errors].size>0
		flash[:error] = response[:errors].join
		redirect_to edit_profile_path
	else
	  cookies.permanent["authenticity_token"]=nil
		reset_session
		flash[:notice] = I18n.t(:delete_final)
		redirect_to welcome_path
	end
  end
  
  def suspend_final
	# inactivate and invalidate account
	# erase session
	# logout
	if params["email"] && !params["email"].eql?(@ses[:email])
		redirect_to welcome_path
		return
	end
	auth_response = MembersAmCafe.suspend_account(params) unless params.nil?
	if auth_response.nil? || auth_response["message"].nil? || auth_response["message"].eql?("false") || !auth_response.instance_of?(Hash)
		flash[:error] = "Unable to suspend account, please try again."
		redirect_to edit_profile_path
	else
		cookies.permanent["authenticity_token"]=nil
		reset_session
		flash[:notice] = I18n.t(:suspend_final).html_safe
		redirect_to welcome_path
	end
  end
  
end
