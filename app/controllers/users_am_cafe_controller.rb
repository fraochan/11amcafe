
class Hash
  def add(key, msg)
    self.merge(key => msg)
  end
end

class UsersAmCafeController < ApplicationController
  include ReCaptcha::AppHelper
  
  
  def show
    response = UsersAmCafe.getUser(params["email"])
	if response[:status].eql?("OK")
	  @user = response[:user]
	else
	  @users = response[:user]
	  @error = response[:errors]
	  redirect_to users_path
	end
  end
  
  def index
    users_db = UsersAmCafe.all
    @users = []
    users_db.each do |user|
      @users << user.attributes.to_h
    end
    @users
  end
  
  def new
    
  end
  
  def create
    error = {}
    @usr = {}
    #join birthdate
    if params[:user] && params[:user]["birthdate(1i)"]
      birthdate = params[:user]["birthdate(1i)"].to_s + '-' + params[:user]["birthdate(2i)"].to_s.rjust(2,'0') + '-' + params[:user]["birthdate(3i)"].to_s.rjust(2,'0')
      params[:user].delete("birthdate(1i)")
      params[:user].delete("birthdate(2i)")
      params[:user].delete("birthdate(3i)")
      params[:user]["birthdate"]=birthdate
    end
    
    #if params[:user] && params[:user]["city"]
      #if params[:user]["city"].include?("All")
      #  params[:user]["city_en"]=["All"]
      #else
      #  value = params[:user]["city"]
      #  if value.instance_of?(String)
      #    value = [] << value 
      #  else #if value.instance_of(Set)
      #    value = value.to_a
      #  end
      #  params[:user]["city_en"] = []
      #  value.each do |city|
      #  id = City.find_by_name_and_locale(city, I18n.locale.to_s.upcase)
      #    if id
      #      id = id.id
      #      params[:user]["city_en"] << City.find_by_id_and_locale(id, "EN").name
      #    end
      #  end
      #end
    #end
    
    #if validate_recap(params, error, {:msg => I18n.t(:recaptcha_failed)})
	    email = params[:user]["email"]
	    password = params[:user]["password"]
  	  response = UsersAmCafe.createUser(params[:user])
	    if response and response[:status].eql?('OK')
		      MembersAmCafe.send_email_validation({:email => email, :password => password})
          flash[:notice] = I18n.t(:registration_correct)
          render :signup_finish#second_step_new_user_path({:email => params[:user]["email"], :from_search => params["from_search"]})
      else
  		  flash[:error] = (I18n.t(:registration_error) + ". <br>" + response[:errors].join("<br>")).html_safe
  		  @usr = params[:user]
  		  render :action => 'new'
      end
    #else
    #  flash[:error] = I18n.t(:recaptcha_failed)
    #  @usr = params[:user]
	  #  render :action => 'new'
    #end
	
  end
  
  def second_step_new_user
    #if (params["from_search"].eql?("new") || params["from_search"].eql?("second_step_new_user")) && !params[:email].nil?
    if refered_from_our_site? && refered_from_user_signup?
      session[:email]=params[:email]
      @member ||= UsersAmCafe.getUser(session[:email])[:user]
      if !@member || !@member["email"]
        redirect_to welcome_path
      end
    else 
      redirect_to welcome_path
    end
  end
  
  def update
    UsersAmCafe.updateUser(params[:user])
    flash[:notice] = I18n.t(:user_update_correct)
    redirect_to users_path
  end
  
  def http_referer_uri
    request.env["HTTP_REFERER"] && URI.parse(request.env["HTTP_REFERER"])
  end

  def refered_from_our_site?
    if uri = http_referer_uri
      uri.host == request.host
    end
  end

  def refered_from_user_signup?
    if refered_from_our_site?
      http_referer_uri.try(:path)['users/new'] || http_referer_uri.try(:path)['users/second_step_new_user'] || http_referer_uri.try(:path)['users/delete_picture_ss']
    end
  end
  
  def edit
    @user = UsersAmCafe.getUser(params["email"])
  end
  
  def destroy
    if UsersAmCafe.deleteUser(params["email"])
      flash[:error]=I18n.t(:user_pictures_delete_error) unless PicturesAmCafe.removeAllPictures(params["email"])
      flash[:notice] = I18n.t(:user_delete_correct) unless flash[:error]
      matches = Match.where("email = ? or match_email= ?", params["email"], params["email"])
      matches.each do |match|
        match.destroy
      end if matches
      @ses = Session.find_by_email(params["email"])
      @ses.destroy if @ses
    else
      flash[:error] = I18n.t(:user_delete_error)
    end
    redirect_to welcome_path
  end
  
  def show
    response = UsersAmCafe.getUser(params["email"])
    @user = response[:user]
    flash[:error]=response[:errors]
  end
  
  def sign_in
    response=UsersAmCafe.sign_in(params["email"],params["password"])
    @user = response[:user]
    flash[:error]=response[:errors].join("<br>")
    if @user && @user.size>0
      # store auth token and email in session
      @ses = Session.new
      @ses.email = params["email"]
      @ses.session_id = params["authenticity_token"]
      if !@ses.save
        flash[:error]=I18n.t(:login_error)
        redirect_to welcome_path({:email => params["email"]})
      else
        cookies.permanent["authenticity_token"] = params["authenticity_token"]
        # redirect to members
        if UsersAmCafe.isAdmin?(params["email"])
          redirect_to admin_profile_path
        else
          redirect_to members_path
        end 
      end
    else
      redirect_to welcome_path({:email => params["email"]})
    end
  end
  
end
