
require 'httparty'
class ElevenAmCafeController < ApplicationController
  respond_to :json
  
  before_filter :check_location
   
  
  def check_location
    #session[:locale] ||= HTTParty.get("http://freegeoip.net/json/"+request.ip)["country_code"]
    session[:locale] = "JA"
    if session[:locale].eql?("JA")
      I18n.default_locale = session[:locale].downcase.to_sym
      I18n.locale = session[:locale].downcase.to_sym
    else
      I18n.default_locale = :en
      I18n.locale = :en
    end
    start_time = DateTime.new(Time.zone.now.year, Time.zone.now.month, Time.zone.now.day, Time.zone.now.hour, Time.zone.now.min, Time.zone.now.sec)#+2.hours
      end_time = DateTime.new(Time.zone.now.year, Time.zone.now.month, Time.zone.now.day, 11, 0, 0)
      if start_time > end_time
        tomorrow = Time.zone.now + 1.day
        end_time = DateTime.new(tomorrow.year, tomorrow.month, tomorrow.day, 11, 0, 0)
      end
      @time_to_rematch = ((end_time - start_time) * 24 * 60 * 60).to_i
  end
  
  def index
    @user = params
    @ses = Session.find_by_session_id(cookies["authenticity_token"]) if cookies["authenticity_token"]
    if @ses && UsersAmCafe.isAdmin?(@ses["email"])
      redirect_to admin_profile_path
    elsif @ses
      redirect_to members_path
    end
  end
  
  def customer_support
    
  end
  
  def send_forgotten_password
    if params["email"].nil? || params["email"].eql?("")
      flash[:error] = I18n.t(:must_provide_email)
      redirect_to forget_password_path
      return
    else
      response = UsersAmCafe.forget_password(params["email"])
      if response[:status].eql?("OK")
        flash[:notice] = I18n.t(:recover_password_email_sent)
      else
        flash[:error] = response[:errors].join
        redirect_to forget_password_path
        return
      end
    end
    redirect_to welcome_path
  end
  
  def forget_password
    
  end
  
  def sweep_old_sessions
    if !params || !params["key"] || !params["key"].eql?("guanteletedelinfinito")
      response = {:message => "false", :status => 404}
      render json: response
      return
    end
    if params && params["time"]
     Session.sweep(params["time"])
    else 
     Session.sweep
    end
    response = {:message => "true", :status => 200}
    render json: response
   end
   
   def get_picture_ss
      url = PicturesAmCafe.getPicture(session[:email],params["name"], params["type"])
      redirect_to url.to_s
   end
   
   def add_picture_ss
     #return unless UsersAmCafe.modifyInformation(session[:email])
     if params["pictures"] && params["pictures"].size <= 5
       params["pictures"].each do |key, picture|
         # add picture & thumbnail
         flash[:error] = I18n.t(:picture_upload_error) unless 
               PicturesAmCafe.addPicture(session[:email],picture,key,{"resize" => "640x480"}) &&
               PicturesAmCafe.addPicture(session[:email],picture,key+"_t",{"resize" => "210x280"})
       end
     end
     flash[:notice] = I18n.t(:picture_upload_correct) unless flash[:error]
     redirect_to second_step_new_user_path({:email => params["email"]})
   end
   
   def delete_picture_ss
     flash[:error] = I18n.t(:picture_upload_error) unless PicturesAmCafe.removePicture(session[:email], params["name"])
     flash[:notice] = I18n.t(:picture_upload_correct) unless flash[:error]
     redirect_to second_step_new_user_path({:email => params["email"]})
   end
   
   def save_user_information_ss
     begin
     if params.nil? || params["advance"].nil?
       flash[:error] = I18n.t(:all_params_error)
       #render :action => 'second_step_new_user'
       redirect_to second_step_new_user_path({:email => params["email"]})
     end
     begin
       min = params["advance"]["min"].to_i if params["advance"]["min"] && !params["advance"]["min"].eql?("")
     rescue StandardError => p
       min = 0
     end
     end

     #check mandatory fields
     for i in 1..min
      params["advance"].each_key { |key|
         if key.index(i.to_s)
           if params["advance"][key].nil? || params["advance"][key].eql?("")
             flash[:error] = I18n.t(:all_params_error)
             @member = params["advance"].to_hash
             #render :action => 'second_step_new_user'
             redirect_to second_step_new_user_path({:email => params["email"]})
             return
           end
         end
      }
     end if min>1

     email = session[:email]
     #params.delete("email")
     response = UsersAmCafe.updateUser(email, params["advance"]) #if UsersAmCafe.modifyInformation(email)
     if response && response[:status].eql?("OK")
       flash[:notice] = I18n.t(:user_update_correct)
     else
       flash[:error] = I18n.t(:user_update_error) + ". " + response[:errors].join.to_s
     end
     redirect_to second_step_new_user_path({:email => params["email"]})
   rescue => p
     redirect_to index_path
   end
   
end