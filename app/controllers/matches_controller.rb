
class MatchesController < ApplicationController
  respond_to :json
  
  before_filter :checkAccess
  
  def checkAccess
    if !params["key"].eql?(@@KEYLOGINCAFE)
      redirect_to "/404.html"
      return
    end
  end
  
  def create
  end
  
  def update
  end
  
  def index
    @matches = Match.all
  end
  
  def new
   # match = Match.find_by_email_and_match_email_and_active(params["email"], params["match_email"], 'A')
   # if match || params["email"].eql?(params["match_email"]) || params["email"].nil? || params["match_email"].nil?
   #   response = {:message => "false", :status=> 200}
   #   render json: response
   # else
	  creation_time = DateTime.new(Time.zone.now.year, Time.zone.now.month, Time.zone.now.day, Time.zone.now.hour, Time.zone.now.min, Time.zone.now.sec)
      match = Match.new({:email => params["email"], :match_email=> params["match_email"], :created_at => creation_time, :updated_at => creation_time}, :without_protection => true)
      if match.save
        match = Match.new({:email => params["match_email"], :match_email=> params["email"], :created_at => creation_time, :updated_at => creation_time}, :without_protection => true)
        if match.save    
          response = {:message => "true", :status=> 200}
          render json: response
        else
          response = {:message => "false", :status=> 200}
          render json: response
        end
      else
        response = {:message => "false", :status=> 200}
        render json: response
      end      
    #end
  end

  def edit
  end
  
  def show
    @matches = Match.where("").order("id desc")
  end
  
  def destroy
    
  end
  
  def delete_match
    if (!params["email"] || !params["match_email"])
      response = {:message => "false", :status=> 200}
      render json: response
      return
    end
    match = Match.find_by_email_and_match_email(params["email"], params["match_email"])
    match.delete if match
    match = Match.find_by_email_and_match_email(params["match_email"], params["email"])
    match.delete if match
    response = {:message => "true", :status=> 200}
    render json: response
  end
  
end