class PaymentsController < ApplicationController
  respond_to :json
  
  before_filter :check_guantelete
  
  def check_guantelete
    if !params[:key].eql?("guanteletedelinfinito")
      redirect_to "/404.html"
    end
  end
  
  def buy_phone
    response = {:message => "true", :status => 200}
    render json: response
  end
end