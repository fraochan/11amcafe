class AdminAmCafeController < ApplicationController
  layout "admin"
  before_filter :checkCredentials
  
  def checkCredentials
    #Session.sweep
    @ses = Session.find_by_session_id(cookies["authenticity_token"]) if cookies["authenticity_token"]
    if @ses.nil? || !UsersAmCafe.isAdmin?(@ses[:email])
      flash[:error] = I18n.t(:login_error)
      cookies.permanent["authenticity_token"]=nil
      reset_session
      redirect_to welcome_path
      return
    end
    @ses.save # update session to keep member logged in (automatically logged off after 7 days of inactivity)
    #session[:active] ||=  MembersAmCafe.is_active(@ses.email)
    #session[:has_match] ||= Match.has_match(@ses.email)
    #start_time = DateTime.now+2.hours
    #end_time = DateTime.new(Time.now.year, Time.now.month, Time.now.day, 11, 0, 0)
    #if start_time > end_time
    #  tomorrow = Time.now + 1.day
    #  end_time = DateTime.new(tomorrow.year, tomorrow.month, tomorrow.day, 11, 0, 0)
    #end
    #session[:end_time]=end_time
    #@time_to_rematch = ((end_time - start_time) * 24 * 60 * 60).to_i
  end
  
  def admin_profile
    @member = UsersAmCafe.getUser(@ses[:email])[:user]
  end
  
  def show_invalid_male_users
    if params && params["email"]
      next_user = params["email"]
    end
    users = UsersAmCafe.getAllToValidate
    @all_users=[]
    users.each do |user|
      #valid_status = MembersAmCafe.validation_status(user)
      @all_users << user if PicturesAmCafe.how_many_pictures?(user)>=2 && UsersAmCafe.has_advance_data?(user) && UsersAmCafe.is_male?(user) #&& !valid_status["email_validation_date"].nil?
      break if @all_users.count>=10
    end
    
    #next_user = UsersAmCafe.getNextToValidate
    next_user ||= @all_users[0] if @all_users && @all_users.size>0 && @all_users[0]
    
    if next_user && !next_user.eql?("")
      @user = UsersAmCafe.getUser(next_user)[:user]
    end
    @user_gender = "male"
    render :show_invalid_users
  end
  
  def show_invalid_female_users
    if params && params["email"]
      next_user = params["email"]
    end
    users = UsersAmCafe.getAllToValidate
    @all_users=[]
    users.each do |user|
      #valid_status = MembersAmCafe.validation_status(user)
      @all_users << user if PicturesAmCafe.how_many_pictures?(user)>=2 && UsersAmCafe.has_advance_data?(user) && !UsersAmCafe.is_male?(user) #&& !valid_status["email_validation_date"].nil?
      break if @all_users.count>=10
    end
    
    #next_user = UsersAmCafe.getNextToValidate
    next_user ||= @all_users[0] if @all_users && @all_users.size>0 && @all_users[0]
    
    if next_user && !next_user.eql?("")
      @user = UsersAmCafe.getUser(next_user)[:user]
    end
    @user_gender = "female"
    render :show_invalid_users
  end
  
  def validate_user
    UsersAmCafe.adminValidateUser(params["email"])
    if params["gender"] && params["gender"].eql?("male") 
      redirect_to show_invalid_male_users_path
      return
    else  
      redirect_to show_invalid_female_users_path
      return
    end
  end
  
  def reject_user
    UsersAmCafe.adminRejectUser(params["email"])
    if params["gender"] && params["gender"].eql?("male") 
      redirect_to show_invalid_male_users_path 
      return
    else  
      redirect_to show_invalid_female_users_path
      return
    end
  end
  
  def get_user_picture
    url = PicturesAmCafe.getPicture(params["email"],params["name"], params["type"])
    redirect_to url.to_s
  end
  
  def save_admin_new_password
    if params["user"].nil? || params["user"]["email"].nil? || params["user"]["password"].nil? || params["user"]["new_password"].nil? ||
      params["user"]["new_password2"].nil? || !params["user"]["new_password"].eql?(params["user"]["new_password2"])
      flash[:error] = I18n.t(:password_change_error)
    else
      if params["user"]["password"].eql?(params["user"]["new_password"])
        flash[:notice] = I18n.t(:password_change_correct)
      else
        auth_response = MembersAmCafe.save_new_password(params["user"])
        if auth_response.nil? || auth_response["message"].nil? || auth_response["message"].eql?("false") || !auth_response.instance_of?(Hash)
          flash[:error] = I18n.t(:password_change_error)
        else
          flash[:notice] = I18n.t(:password_change_correct)
        end
      end
    end
    redirect_to admin_profile_path
  end
  
end